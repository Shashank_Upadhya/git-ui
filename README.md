# [Visual Git](https://gitlab.com/mountblue/august-18-js/visual-git-using-electron)<img src="http://icons.iconarchive.com/icons/goodstuff-no-nonsense/free-space/64/astronaut-icon.png" />



[![Travis](https://img.shields.io/travis/desktop/desktop.svg?style=flat-square&label=Travis+CI)](https://travis-ci.org/desktop/desktop)
[![CircleCI](https://img.shields.io/circleci/project/github/desktop/desktop.svg?style=flat-square&label=CircleCI)](https://circleci.com/gh/desktop/desktop)
[![AppVeyor Build Status](https://img.shields.io/appveyor/ci/github-windows/desktop/master.svg?style=flat-square&label=AppVeyor&logo=appveyor)](https://ci.appveyor.com/project/github-windows/desktop/branch/master)
[![Azure DevOps Pipelines Build Status](https://dev.azure.com/github/Desktop/_apis/build/status/Continuous%20Integration)](https://dev.azure.com/github/Desktop/_build/latest?definitionId=3)
[![license](https://img.shields.io/github/license/desktop/desktop.svg?style=flat-square)](https://github.com/desktop/desktop/blob/master/LICENSE)
![90+% TypeScript](https://img.shields.io/github/languages/top/desktop/desktop.svg?style=flat-square&colorB=green)

Visual Git is an open source [Electron](https://electron.atom.io)-based
Visual Git app. It is written in [Javascript](http://es6-features.org/#Constants) and
uses [React](https://facebook.github.io/react/).

![GitHub Desktop screenshot - Windows](https://cloud.githubusercontent.com/assets/359239/26094502/a1f56d02-3a5d-11e7-8799-23c7ba5e5106.png)


## Where can I get it?

Download the official installer for your operating system:

- [macOS](https://central.github.com/deployments/desktop/desktop/latest/darwin)
- [Windows](https://central.github.com/deployments/desktop/desktop/latest/win32)
- [Windows machine-wide install](https://central.github.com/deployments/desktop/desktop/latest/win32?format=msi)


There are several community-supported package managers that can be used to install Github Desktop.
- Windows users can install using [Chocolatey](https://chocolatey.org/) package manager:
     `c:\> choco install github-desktop`
- macOS users can install using [Homebrew](https://brew.sh/) package manager:
     `$ brew cask install github`
- Arch Linux users can install the latest version from the [AUR](https://aur.archlinux.org/packages/github-desktop/).

You can install this alongside your existing GitHub Desktop for Mac or GitHub
Desktop for Windows application.

**NOTE**: there is no current migration path to import your existing
repositories into the new application - you can drag-and-drop your repositories
from disk onto the application to get started.
